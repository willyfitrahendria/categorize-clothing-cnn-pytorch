# Categorize Clothing CNN PyTorch

categorize clothing using CNN (PyTorch) on Fashion-MNIST dataset.

Support GPU implementation

Python 3.7.3

PyTorch 1.1.0

Accuracy
---

![](./output/accuracy.png)